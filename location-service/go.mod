module gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/location-service

go 1.14

require (
	github.com/google/wire v0.4.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/prometheus/common v0.4.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.6.2
	gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/pkg/transport v0.0.0-20200322082814-f8a9826032ab
	golang.org/x/sys v0.0.0-20200321134203-328b4cd54aae // indirect
)
