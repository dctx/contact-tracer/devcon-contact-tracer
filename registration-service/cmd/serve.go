package cmd

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/dctx/devcon-contact-tracer/registration-service/internal/server"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "run the serve",
	Run:   runServe,
}

func init() {
	rootCmd.AddCommand(serveCmd)
}

func runServe(c *cobra.Command, args []string) {
	log.Info("Starting registration-service...")
	if err := server.Run(); err != nil {
		log.WithError(err).Error("registration-service terminated")
		os.Exit(1)
	}
}
