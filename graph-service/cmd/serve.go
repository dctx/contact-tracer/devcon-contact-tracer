package cmd

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "run the serve",
	Run:   runServe,
}

func init() {
	rootCmd.AddCommand(serveCmd)
}

func runServe(c *cobra.Command, args []string) {
	log.Info("Starting graph-service...")
	apiServer, err := createServer()
	if err != nil {
		log.WithError(err).Error("graph-service terminated")
		os.Exit(1)
	}

	if err := apiServer.Run(); err != nil {
		log.WithError(err).Error("graph-service terminated")
		os.Exit(1)
	}
}
