package handler

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/dctx/devcon-contact-tracer/graph-service/internal/domain/checkin"
)

type CheckinHandler struct {
	*mux.Router
	checkin.CheckinService
}

func setHeaders(w *http.ResponseWriter) {
	handler := *w
	handler.Header().Set("Content-Type", "application/json")
	handler.Header().Set("Access-Control-Allow-Origin", "*")
}

func NewCheckinHandler(r *mux.Router, service checkin.CheckinService) *CheckinHandler {
	handler := &CheckinHandler{
		Router:         r,
		CheckinService: service,
	}
	handler.register()
	return handler
}

func (h *CheckinHandler) register() {
	checkinRoute := h.PathPrefix("/api/v1/checkins").Subrouter()

	checkinRoute.HandleFunc("/", h.listCheckins()).Methods(http.MethodGet)
	checkinRoute.HandleFunc("/", h.checkIn()).Methods(http.MethodPost)

	// add more routes here
}

func (CheckinHandler *CheckinHandler) checkIn() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {

	}
}

func (CheckinHandler *CheckinHandler) listCheckins() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		setHeaders(&res)
		checkins, err := CheckinHandler.ListCheckIn()

		if err != nil {
			http.Error(res, err.Error(), 400)
			return
		}

		json.NewEncoder(res).Encode(checkins)
		return
	}
}
