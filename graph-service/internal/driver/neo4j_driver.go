package driver

import (
	"fmt"

	"github.com/neo4j/neo4j-go-driver/neo4j"
	"github.com/spf13/viper"
)

func CreateDriver() (neo4j.Driver, error) {
	useConsoleLogger := func(level neo4j.LogLevel) func(config *neo4j.Config) {
		return func(config *neo4j.Config) {
			config.Log = neo4j.ConsoleLogger(level)
		}
	}

	fmt.Println(viper.GetString("neo4j.url"))
	fmt.Println(viper.GetString("neo4j.username"))
	fmt.Println(viper.GetString("neo4j.password"))

	driver, err := neo4j.NewDriver(viper.GetString("neo4j.url"), neo4j.BasicAuth(viper.GetString("neo4j.username"), viper.GetString("neo4j.password"), ""), useConsoleLogger(neo4j.DEBUG))
	return driver, err
}
