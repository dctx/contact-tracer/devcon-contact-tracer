package main

import "gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/cmd"

var version = "dev"

func main() {
	cmd.Execute(version)
}
