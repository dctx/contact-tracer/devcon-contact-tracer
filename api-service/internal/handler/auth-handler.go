package handler

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/service"
)

type AuthHandler struct {
	*service.AuthService
}

func NewAuthHandler(s *service.AuthService) *AuthHandler {
	handler := &AuthHandler{AuthService: s}
	return handler
}

func (h *AuthHandler) Register(router *mux.Router) {
	authRoute := router.PathPrefix("/auth")
	authRoute.
		Methods(http.MethodPost, http.MethodOptions).
		Path("/register").
		Handler(register(h.AuthService))
	log.Info("[POST] /auth/register registered")
}

func register(authService *service.AuthService) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		// get request

		// call authService
		if err := authService.Register(nil); err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

		res.WriteHeader(http.StatusCreated)
	}

}
