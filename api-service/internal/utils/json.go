package utils

import (
	"encoding/json"

	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/model/errors"
)

func FromJSON(in []byte, out interface{}) errors.JSONErrors {
	err := json.Unmarshal(in, out)
	if err != nil {
		return errors.New().Add(
			"400",
			map[string]string{"pointer": "/data"},
			"Request cannot be read as JSON",
			err.Error())
	}
	return nil
}

func ToJSON(in interface{}) ([]byte, errors.JSONErrors) {
	out, err := json.Marshal(in)
	if err != nil {
		return nil, errors.New().Add(
			"500",
			map[string]string{"pointer": "/data"},
			"Response cannot be encoded as JSON",
			err.Error())
	}
	return out, nil
}
