package service

import (
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/model"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/model/errors"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/utils"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/pkg/transport"
)

const CommandChannel = "dctx.contact-tracer.api"

type LocationService struct {
	transport.MessageBus
}

func NewLocationService(mb transport.MessageBus) *LocationService {
	return &LocationService{mb}
}

func (s *LocationService) Checkin(checkin *model.CheckInRequest) (*model.CheckInResponse, errors.JSONErrors) {

	var res *model.CheckInResponse
	var err errors.JSONErrors

	switch checkin.CheckInType {
	case model.CheckInTypeLatLong:
		res, err = s.checkinLatLong()
	case model.CheckInTypePOI:
		res, err = s.checkinPOI()
	}

	return res, err
}

func (s *LocationService) checkinLatLong() (*model.CheckInResponse, errors.JSONErrors) {
	reqID := utils.NewUUID()

	msg := transport.Message{
		Key:   []byte("test"),
		Value: []byte("again"),
	}
	err := s.Publish(CommandChannel, msg)
	if err != nil {
		return nil, errors.New().Add("500", map[string]string{}, "Request not sent to processor", err.Error())
	}
	return &model.CheckInResponse{reqID}, nil
}

func (s *LocationService) checkinPOI() (*model.CheckInResponse, errors.JSONErrors) {

	return &model.CheckInResponse{utils.NewUUID()}, nil
}
